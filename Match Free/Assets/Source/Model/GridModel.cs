﻿
using System.Collections.Generic;
using UnityEngine;

public class Tuple<T1, T2>
{
    public T1 Value1;
    public T2 Value2;

    public Tuple(T1 value1, T2 value2)
    {
        Value1 = value1;
        Value2 = value2;
    }
}

public class GridModel
{
    public int[,] Grid { get; private set; }

    public Vector2Int GridSize { get; private set; }
    private int _cellTypesCount;

    private int _matchCellsAmount = 3;
    
    /// <summary>
    /// cellTypesCount includes 0 - no cell
    /// </summary>
    public GridModel(Vector2Int gridSize, int cellTypesCount)
    {
        GridSize = gridSize;
        _cellTypesCount = cellTypesCount;
        
        Grid = new int[gridSize.x, gridSize.y];
        
        GenerateRandomGrid();
    }

    public void SwapCells(Vector2Int one, Vector2Int two)
    {
        var tmp = Grid[one.x, one.y];
        Grid[one.x, one.y] = Grid[two.x, two.y];
        Grid[two.x, two.y] = tmp;
    }
    
    public List<Vector2Int> CalcRemovedCells()
    {
        var cells = new List<Vector2Int>();
        var visited = new HashSet<Vector2Int>();

        for (int y = 0; y < GridSize.y; y++)
        {
            for (int x = 0; x < GridSize.x; x++)
            {
                var current = new Vector2Int(x, y);
                var currentType = Grid[x, y];
                if (visited.Contains(current)) continue;
                
                var que = new Queue<Vector2Int>();
                que.Enqueue(current);

                var removeGroup = new List<Vector2Int>();
                
                while (que.Count > 0)
                {
                    var groupCell = que.Dequeue();
                    visited.Add(groupCell);
                    removeGroup.Add(groupCell);

                    var sameCellsNear = GetNeigborSameCells(groupCell.x, groupCell.y, currentType);
                    foreach (var sameCell in sameCellsNear)
                    {
                        if (visited.Contains(sameCell)) continue;
                        //removeGroup.Add(sameCell);
                        que.Enqueue(sameCell);
                    }

                    if (removeGroup.Count >= _matchCellsAmount)
                    {
                        cells.AddRange(removeGroup);
                    }
                }
            }
        }

        foreach (var cell in cells)
        {
            Grid[cell.x, cell.y] = 0;
        }
        
        return cells;
    }

    public List<Tuple<Vector2Int, Vector2Int>> CalcFallDownCells()
    {
        var fallenCells = new List<Tuple<Vector2Int, Vector2Int>>();

        for (int y = GridSize.y - 1; y >= 0; y--)
        {
            for (int x = 0; x < GridSize.x; x++)
            {
                var fallY = FallY(x, y);

                if (fallY != y)
                {
                    SwapCells(new Vector2Int(x, y), new Vector2Int(x, fallY));
                    fallenCells.Add(new Tuple<Vector2Int, Vector2Int>(new Vector2Int(x, y), new Vector2Int(x, fallY)));                    
                }
            }
        }        
        
        return fallenCells;
    }

    private int FallY(int x, int y)
    {
        int fallY = y + 1;

        if (GetCellOrZero(x, fallY) > 0 || fallY >= GridSize.y)
        {
            return y;
        }
        else
        {
            return FallY(x, fallY);
        }
    }    
    
    private void GenerateRandomGrid()
    {
        for (int y = 0; y < GridSize.y; y++)
        {
            for (int x = 0; x < GridSize.x; x++)
            {
                int rnd = Random.Range(1, _cellTypesCount);

                while (GetNeigborSameCells(x, y, rnd).Count > 0)
                {
                    rnd = Random.Range(1, _cellTypesCount);
                }
                
                Grid[x, y] = rnd;
            }
        }
    }
    
    private int GetCellOrZero(int x, int y)
    {
        if (x < 0 || y < 0 || x >= GridSize.x || y >= GridSize.y) return 0;
        return Grid[x, y];
    }
    
    private List<Vector2Int> GetNeigborSameCells(int x, int y, int cellType)
    {
        var cells = new List<Vector2Int>();

        AddCellIfSame(x - 1, y, cellType, cells);
        AddCellIfSame(x, y - 1, cellType, cells);
        AddCellIfSame(x + 1, y, cellType, cells);
        AddCellIfSame(x, y + 1, cellType, cells);
        
        return cells;
    }

    private void AddCellIfSame(int x, int y, int cellType, List<Vector2Int> cells)
    {
        var cell = GetCellOrZero(x, y);
        if (cell == 0) return;

        if (cell == cellType)
        {
            cells.Add(new Vector2Int(x, y));
        }
    }
}
