﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class CellView : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private SpriteRenderer _sprite;

    [SerializeField]
    private Collider2D _cellCollider;
    
    private float _normalSize = 5f;
    private float _selectedSize = 6f;

    public Vector2Int Cell;// { get; private set; }
    public Action<CellView> OnCellClick;
    
    public void Init(Color color, Vector2Int cell, Vector3 position)
    {
        Cell = cell;
        gameObject.transform.localPosition = position;
        _sprite.gameObject.transform.localScale = new Vector3(_normalSize, _normalSize, 1);
        SetColor(color);
        _cellCollider.enabled = true;
    }

    public void SetColor(Color color)
    {
        _sprite.color = color;
    }

    public void SetSelected(bool selected, Action<CellView> animationFinishCallback = null)
    {
        _sprite.sortingOrder = selected ? 1 : 0;
        var scaleTarget = selected ? _selectedSize : _normalSize;
        _sprite.gameObject.transform.DOScale(scaleTarget, 0.5f).onComplete = () => {
            if (animationFinishCallback != null)
            {
                animationFinishCallback.Invoke(this);
            }
        };
    }

    public void MoveCell(Vector3 newPosition, Vector2Int newCell, Action<CellView> animationFinishCallback = null)
    {
        _sprite.sortingOrder = 1;
        Cell = newCell;
        gameObject.transform.DOLocalMove(newPosition, 0.5f).onComplete = () => {
            _sprite.sortingOrder = 0;
            if (animationFinishCallback != null)
            {
                animationFinishCallback.Invoke(this);
            }
        };
    }

    public void HideCell(Action<CellView> animationFinishCallback = null)
    {
        _cellCollider.enabled = false;
        var seq = DOTween.Sequence();
        seq.Append(_sprite.transform.DOShakeRotation(0.5f));
        seq.Insert(0, _sprite.transform.DOScale(0f, 0.5f));
        seq.onComplete = () =>
        {
            if (animationFinishCallback != null)
            {
                animationFinishCallback.Invoke(this);
            }
        };
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        if (OnCellClick != null)
        {
            OnCellClick.Invoke(this);
        }
    }
}
