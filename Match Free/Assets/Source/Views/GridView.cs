﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class GridView : MonoBehaviour 
{
    [SerializeField]
    private CellView _cellPrefab;

    [SerializeField] 
    private float _cellSize;

    [SerializeField]
    private Color[] _colors;
    
    private Vector2Int _gridSize;

    private CellView[,] _cells;

    private CellView _selectedCell;

    public Action<Vector2Int, Vector2Int> OnSwapCells;

    public GridModel Model;
    
    public void Init(Vector2Int gridSize, int[,] grid)
    {
        _gridSize = gridSize;
        _cells = new CellView[_gridSize.x, _gridSize.y];
        
        InitGrid(grid);
    }

    public void SetCell(Vector2Int cell, int cellType)
    {
        if (_cells == null) return;

        var cellView = _cells[cell.x, cell.y];

        if (cellView == null)
        {
            cellView = Instantiate(_cellPrefab, gameObject.transform);
            _cells[cell.x, cell.y] = cellView;
        }
        
        cellView.Init(_colors[cellType], cell, new Vector3(cell.x * _cellSize, cell.y * _cellSize, 0));
        cellView.OnCellClick = OnCellClick;
    }
    
    public void SwapCells(Vector2Int one, Vector2Int two)
    {
        var cellOne = _cells[one.x, one.y];
        var cellTwo = _cells[two.x, two.y];

        _cells[one.x, one.y] = cellTwo;
        _cells[two.x, two.y] = cellOne;
        
        cellOne.MoveCell(new Vector3(two.x * _cellSize, two.y * _cellSize, 0), two);
        cellTwo.MoveCell(new Vector3(one.x * _cellSize, one.y * _cellSize, 0), one, view =>
        {
            if (OnSwapCells != null)
            {
                OnSwapCells.Invoke(one, two);
            }
        });
    }

    public void HideCell(Vector2Int cell)
    {
        _cells[cell.x, cell.y].HideCell(view =>
        {
            //Destroy(_cells[cell.x, cell.y]);
            _cells[cell.x, cell.y] = null;
        });
    }

    public void MoveCell(Vector2Int oldCell, Vector2Int newCell)
    {
        _cells[oldCell.x, oldCell.y].MoveCell(new Vector3(newCell.x * _cellSize, newCell.y * _cellSize, 0), newCell);
        _cells[newCell.x, newCell.y] = _cells[oldCell.x, oldCell.y];
        _cells[oldCell.x, oldCell.y] = null;
    }
    
    private void InitGrid(int[,] grid)
    {
        for (var y = 0; y < _gridSize.y; y++)
        {
            for (var x = 0; x < _gridSize.x; x++)
            {
                SetCell(new Vector2Int(x, y), grid[x, y]);
            }
        }
    }

    private void OnCellClick(CellView cellView)
    {
        if (_selectedCell == null)
        {
            _selectedCell = cellView;
            cellView.SetSelected(true);
        }
        else
        {
            SwapCells(_selectedCell.Cell, cellView.Cell);
            _selectedCell.SetSelected(false);
            _selectedCell = null;
        }
    }

    private void OnDrawGizmos()
    {
        for (var y = 0; y < _gridSize.y; y++)
        {
            for (var x = 0; x < _gridSize.x; x++)
            {
                if ( _cells[x,y] != null) Handles.Label(gameObject.transform.position + new Vector3(x * _cellSize, y * _cellSize, 0), _cells[x,y].Cell.ToString());
            }
        }        
    }
}
