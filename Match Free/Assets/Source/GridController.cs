﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController
{
    private GridView _gridView;
    private GridModel _model;

    public GridController(GridModel model, GridView gridView)
    {
        _model = model;
        _gridView = gridView;
        
        _gridView.Init(_model.GridSize, _model.Grid);
        _gridView.OnSwapCells = OnSwapCells;

        _gridView.Model = _model;
    }

    private void OnSwapCells(Vector2Int one, Vector2Int two)
    {
        _model.SwapCells(one, two);
        RemoveCells();
    }

    private void RemoveCells()
    {
        var removed = _model.CalcRemovedCells();
        if (removed.Count == 0) return;

        foreach (var cell in removed)
        {
            _gridView.HideCell(cell);
        }
        
        //FallCells();
    }

    private void FallCells()
    {
        var falled = _model.CalcFallDownCells();
        if (falled.Count == 0) return;
        
        foreach (var tuple in falled)
        {
            _gridView.MoveCell(tuple.Value1, tuple.Value2);
        }
        
        RemoveCells();
    }
}
