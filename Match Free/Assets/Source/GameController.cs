﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
	[SerializeField]
	private Vector2Int _gridSize;

	[SerializeField]
	private int _cellTypesCount;
	
	[SerializeField]
	private GridView _gridView;
	private GridController _gridController;
	
	private GridModel _model;
	
	private void Awake() 
	{
		_model = new GridModel(_gridSize, _cellTypesCount);
		_gridController = new GridController(_model, _gridView);
	}
}
